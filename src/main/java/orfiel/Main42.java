package orfiel;

//import klasy Scanner

import java.util.Scanner;

public class Main42 {

    public static void main(String[] arg) {

        //deklaracja zmiennej "liczba" i przypisanie jej wartości
        int liczba = 0;

        //dodanie obiektu "sc" odbierającego dane wprowadzone przez użytkownika
        Scanner sc = new Scanner(System.in);

        //uruchomienie pętli
        while (true) {

            //prośba o liczbę
            System.out.println("Podaj liczbę całkowitą");

            //nadpisanie wartości "liczba" przez wartość wprowadzoną przez użytkownika
            liczba = sc.nextInt();

            //instrukcja warunkowa kończąca pętle gdy użytkownik wpisze 42
            if (liczba == 42) {

                //gdy zajdzie warunek z if pętla jestrzerywana
                break;
            }
            //wypisanie warości podanej przez użytkownika
            System.out.println(liczba);

            //warunek zakończenia działania pętli i zamknię programu gdy pojawi się liczba 42
        }
    }
}
